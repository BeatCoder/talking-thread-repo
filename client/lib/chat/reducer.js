import * as types from "./actionsTypes";

const initialState = {
  messages: [],
};

export default function(state = initialState, action) {
  switch (action.type) {
    case types.SEND_MESSAGE: {
      const messages = state.messages.concat({
        msgBody: action.msgBody,
        author: action.author,
      });
      return { ...state, messages };
    }
    case types.MESSAGE_RECEIVED: {
      const messages = state.messages.concat({
        msgBody: action.message,
        author: action.user,
      });
      return { ...state, messages };
    }

    case types.RECIEVED_MESSAGES_LIST: {
      return {
        ...state,
        messages: action.messages.map(message => ({
          msgBody: message.message,
          author: message.user,
        })),
      };
    }
    default:
      return state;
  }
}
