import { connect } from "react-redux";
import { push } from "connected-react-router";
import MessageList from "../components/MessageList";

const mapStateToProps = state => ({
  messages: state.chat.messages,
});

export default connect(
  mapStateToProps,
  {
    push,
  },
)(MessageList);
