import { OPEN_SECTION } from "../actionTypes";

const initialState = {
  section: null,
};

export default function(state = initialState, action) {
  switch (action.type) {
    case OPEN_SECTION:
      return { ...state, section: action.section };

    default:
      return state;
  }
}
