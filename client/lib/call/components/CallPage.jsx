// eslint-disable-next-line import/no-extraneous-dependencies
import debounce from "lodash/debounce";
import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";

import ToolBar from "../containers/ToolBar";
import VideoHolder from "../containers/VideoHolder";
import TemporaryVideo from "./TemporaryVideo";

import ToolBox from "../containers/ToolBox";
import { LoginDialog } from "../../login";
import Init from "../../init";

import "../styles.scss";

export default class CallPage extends Component {
  state = {
    isToolBoxVisible: false,
    isToolBarVisible: false,
    hideToolBarId: null,
    hideToolBoxId: null,
  };

  componentWillUnmount() {
    clearTimeout(this.state.showTimeout);
    return null;
  }

  hideToolBar = () => {
    this.setState({
      isToolBarVisible: false,
    });
  };

  hideToolBox = () => {
    if (!this.props.section) {
      this.setState({
        isToolBoxVisible: false,
      });
    }
  };

  showToolElements = () => {
    this.setState({
      isToolBarVisible: true,
    });
    clearTimeout(this.state.hideToolBarId);
    this.setState({ hideToolBarId: setTimeout(this.hideToolBar, 3000) });

    this.setState({
      isToolBoxVisible: true,
    });
    clearTimeout(this.state.hideToolBoxId);
    this.setState({ hideToolBoxId: setTimeout(this.hideToolBox, 3000) });
  };

  // eslint-disable-next-line react/sort-comp
  debounceShowToolElements = debounce(this.showToolElements, 100, {
    leading: true,
  });

  render() {
    const { user } = this.props;
    const isModalOpened = !user.login;
    return (
      <Init>
        <Fragment>
          <LoginDialog isModalOpened={isModalOpened} />
          <div id="logged">
            <div id="session" onMouseMove={this.debounceShowToolElements}>
              <ToolBar visible={this.state.isToolBarVisible} />
              {user.login ? <VideoHolder /> : <TemporaryVideo />}
              <ToolBox visible={this.state.isToolBoxVisible} />
            </div>
          </div>
        </Fragment>
      </Init>
    );
    /* return (
      <Init>
        <Fragment>
          <LoginDialog isModalOpened={isModalOpened} />
          <div id="logged">
            <div id="session" onMouseMove={toolElements}>
              <ToolBar visible={isVisibleBar} />
              {user.login ? <VideoHolder /> : <TemporaryVideo />}
              <ToolBox visible={isVisibleBox} />
            </div>
          </div>
        </Fragment>
      </Init>
    ); */
  }
}

CallPage.propTypes = {
  user: PropTypes.shape({
    login: PropTypes.string,
    password: PropTypes.string,
    publisher: PropTypes.bool,
  }).isRequired,
  section: PropTypes.string,
};
