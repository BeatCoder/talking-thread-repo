function generateSessionName() {
  const adjectives = [
    "Unexpected",
    "Absurd",
    "Dreamy",
    "Huge",
    "Grandiose",
    "Fabulous",
  ];
  const nouns = ["Mushroom", "Whale", "Ant", "Pigeon", "Mouse", "Beaver"];
  const verbs = ["Appear", "Join", "Draw", "Travel", "Сritique", "Speak"];
  const adverbs = [
    "Instantly",
    "Constantly",
    "Loudly",
    "Decidedly",
    "Patiently",
    "Carelessly",
  ];

  function random(min, max) {
    return Math.round(min - 0.5 + Math.random() * (max - min + 1));
  }

  const sessionName = "";
  const newSessionName = sessionName.concat(
    adjectives[random(0, 5)],
    nouns[random(0, 5)],
    verbs[random(0, 5)],
    adverbs[random(0, 5)],
  );
  return newSessionName;
}

export default generateSessionName;
