const validator = require("joi");
const schema = require("../../validators/RoomValidator");

class RoomValidator {
  async execute(room) {
    const result = await validator.validate(room, schema);
    if (result.error) {
      return false;
    }
    return true;
  }
}

module.exports = new RoomValidator();
