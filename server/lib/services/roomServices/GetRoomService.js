const database = require("../../database");

class GetRoom {
  async execute(name) {
    let result = false;
    try {
      const room = await database.Room.findOne({
        where: {
          name,
        },
      });
      if (room) {
        result = room;
      } else {
        console.log("No such room");
      }
    } catch (err) {
      console.log(err);
    }
    return result;
  }
}

module.exports = GetRoom;
